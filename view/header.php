<?php

error_reporting(E_WARNING | E_PARSE);
ini_set('display_errors', 1);
date_default_timezone_set('UTC');

require_once('../model/db.php');
//require_once('../model/user_db.php');

$home_class = ($page == 'home') ? 'current' : '';
$events_class = ($page == 'events') ? 'current' : '';
$gigs_class = ($page == 'gigs') ? 'current' : '';
$bands_class = ($page == 'bands') ? 'current' : '';
$venues_class = ($page == 'venues') ? 'current' : '';

?>

<!DOCTYPE html>

<head>
	<title>Gig Searcher</title>
	<link href="https://fonts.googleapis.com/css?family=Roboto|Slabo+27px" rel="stylesheet">
	<link href="../view/css/reset.css" type="text/css" rel="stylesheet" />
	<link href="../view/css/main.css" type="text/css" rel="stylesheet" />
</head>

<body>
	
	<!--<header>
		
		<h1><a href="../.">Gig Searcher</a></h1>
		
		<form method="get" action="../home/.">
			<input type="hidden" name="action" value="search">
			<input type="search" name="query" placeholder="Search">
			<input type="submit" value="Go">
		</form>
		
		<div class="clear"></div>
		
	</header>-->
	
	<main>
	
		<aside>
			
			<?php if ($me) { ?>
			
			<div id="user-panel">
				<h4>
					<?php echo $me['user_name']; ?>
					<img src="<?php echo get_gravatar($me['user_email'], 56); ?>">
				</h4>
				<p><a href="../home/?action=logout">Logout</a></p>
			</div>
			
			<?php } else { ?>
			
			<div id="login">
				
				<h4>Login</h4>
				
				<form action="../home/." method="post" class="std-form">
					<input type="hidden" name="action" value="login">
					<div><input type="text" name="user_name" placeholder="Username"></div>
					<div><input type="password" name="user_password" placeholder="Password"></div>
					<div>
						<input type="submit" value="Login">
						<span>&nbsp;or&nbsp;</span>
						<a href="../home/?action=register">Register</a>
					</div>
				</form>
				
			</div>
			
			<?php } ?>
			
			<nav>
				<ul>
					<li><a class="<?php echo $home_class; ?>" href="../home/">Home</a></li>
					<li><a class="<?php echo $events_class; ?>" href="../events/">Events</a></li>
					<li><a class="<?php echo $gigs_class; ?>" href="../gigs/">Gigs</a></li>
					<li><a class="<?php echo $bands_class; ?>" href="../bands/">Bands</a></li>
					<li><a class="<?php echo $venues_class; ?>" href="../venues/">Venues</a></li>
				</ul>
			</nav>
			
		</aside>
	
		<div id="content">