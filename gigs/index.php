<?php

/**
 *	controller for gigs
 */

$page = 'gigs';

require_once('../model/db.php');
require_once('../model/user_db.php');
require_once('../model/band_db.php');
require_once('../model/venue_db.php');
require_once('../model/gig_db.php');
require_once('../model/event_db.php');

$action = filter_input(INPUT_POST, 'action');
if ($action === NULL) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action === NULL) {
		$action = 'disp_gigs';
    }
}

if ($action == 'disp_gigs') {
	
	$gigs = get_gigs();
	// get all bands the user has at least booking permission
	$user_bands = get_bands_by_user($me['user_id'], 2);
	
	for ($i = 0; $i < count($gigs); $i++) {
		$gigs[$i]['is_booked'] = is_gig_booked($gigs[$i]['gig_id']);
	}
	
	include('disp_gigs.php');
	
} else if ($action == 'add_gig_form') {
	
	$months = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August',
		'September', 'October', 'November', 'December');
	
	// get all venues that the current user has at least permission to add gigs
	$user_venues = get_venues_by_user($me['user_id'], 2);
	
	if ($me && !$user_venues) {
		die('you do not have permission to add gigs for any venues');
	}
	
	include('add_gig.php');
	
} else if ($action == 'add_gig') {
	
	$venue_id = filter_input(INPUT_POST, 'gig_venue_id');
	$gigname = filter_input(INPUT_POST, 'gig_name');
	$description = filter_input(INPUT_POST, 'gig_description');
	$payout = filter_input(INPUT_POST, 'gig_payout');
	$day = filter_input(INPUT_POST, 'gig_day');
	$month = filter_input(INPUT_POST, 'gig_month');
	$year = filter_input(INPUT_POST, 'gig_year');
	$hour = filter_input(INPUT_POST, 'gig_hour');
	$minute = filter_input(INPUT_POST, 'gig_minute');
	
	if ($venue_id == null || $venue_id == false ||
		$payout == null || $payout == false ||
		$day == null || $day == false ||
		$month == null || $month == false ||
		$year == null || $year == false ||
		$hour == null || $hour == false ||
		$minute == null || $minute == false
	) {
		die('invalid data (something may have been left blank)');
	}
	
	if (!$me) {
		die('must be logged in to add gigs');
	}
	
	if (!checkdate($month, $day, $year)) {
		echo "month: $month<br />day: $day<br />year: $year<br />";
		die('date is invalid');
	}
	
	$datetime = "$year-$month-$day $hour:$minute:00";
	$timestamp = strtotime($datetime);
	
	add_gig($venue_id, $gigname, $description, $payout, $timestamp);
	
	header('Location: .');
	
} else if ($action == 'edit_gig') {
	
	$gig_id = filter_input(INPUT_GET, 'gig_id', FILTER_VALIDATE_INT);
	
	if ($gig_id == null || $gig_id == false) {
		die('invalid gig id');
	}
	
	$gig = get_gig_by_id($gig_id);
	$permission_level = get_venue_permission_level($me['user_id'], $gig['gig_venue_id']);
	
	// check if the user is at least an admin of the venue
	if ($permission_level < 3) {
		$error = 'you don\'t have permission to edit this gig';
		include('../view/error.php');
		die();
	}
	
	$months = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August',
		'September', 'October', 'November', 'December');
	
	// get all venues that the current user has at least permission to add gigs
	$user_venues = get_venues_by_user($me['user_id'], 3);
	
	if ($me && !$user_venues) {
		die('you do not have permission to edit gigs for this venue');
	}
	
	$gig_month = date('n', $gig['gig_date']);
	$gig_day = date('j', $gig['gig_date']);
	$gig_year = date('Y', $gig['gig_date']);
	$gig_hour = date('G', $gig['gig_date']);
	$gig_minute = date('i', $gig['gig_date']);
	
	include('edit_gig.php');
	
} else if ($action == 'save_gig') {
	
	$gig_id = filter_input(INPUT_POST, 'gig_id', FILTER_VALIDATE_INT);
	
	if ($gig_id == null || $gig_id == false) {
		die('invalid gig id');
	}
	
	$venue_id = filter_input(INPUT_POST, 'gig_venue_id');
	$gigname = filter_input(INPUT_POST, 'gig_name');
	$description = filter_input(INPUT_POST, 'gig_description');
	$payout = filter_input(INPUT_POST, 'gig_payout');
	$day = filter_input(INPUT_POST, 'gig_day');
	$month = filter_input(INPUT_POST, 'gig_month');
	$year = filter_input(INPUT_POST, 'gig_year');
	$hour = filter_input(INPUT_POST, 'gig_hour');
	$minute = filter_input(INPUT_POST, 'gig_minute');
	
	if ($venue_id == null || $venue_id == false ||
		$payout == null || $payout == false ||
		$day == null || $day == false ||
		$month == null || $month == false ||
		$year == null || $year == false ||
		$hour == null || $hour == false ||
		$minute == null || $minute == false
	) {
		die('invalid data (something may have been left blank)');
	}
	
	if (!$me) {
		die('must be logged in to add gigs');
	}
	
	if (!checkdate($month, $day, $year)) {
		echo "month: $month<br />day: $day<br />year: $year<br />";
		die('date is invalid');
	}
	
	$datetime = "$year-$month-$day $hour:$minute:00";
	$timestamp = strtotime($datetime);
	
	save_gig($gig_id, $venue_id, $gigname, $description, $payout, $timestamp);
	
	header('Location: .');
	
} else if ($action == 'book_gig') {
	
	$gig_id = filter_input(INPUT_GET, 'gig_id', FILTER_VALIDATE_INT);
	
	// get all bands that the current user has at least permission to book gigs
	$user_bands = get_bands_by_user($me['user_id'], 2);
	
	if ($me && !$user_bands) {
		die('you do not have permission to book gigs for any bands');
	}
	
	// if user has one band, book the gig for that band, otherwise the user selects which band 
	if (count($user_bands) == 1) {
		book_gig($gig_id, $user_bands[0]['band_id']);
	} else {
		// include band selection page
	}
	
	header('Location: .');
	
} else if ($action == 'delete_gig') {
	
	$gig_id = filter_input(INPUT_GET, 'gig_id', FILTER_VALIDATE_INT);
	
	if ($gig_id == null || $gig_id == false) {
		die('invalid gig id');
	}
	
	$gig = get_gig_by_id($gig_id);

	$permission_level = get_venue_permission_level($me['user_id'], $gig['venue_id']);
	
	// check if the user is an admin of the gig
	if ($permission_level < 3) {
		$error = 'you don\'t have permission to delete this gig';
		include('../view/error.php');
		die();
	}
	
	delete_gig($gig_id);
	
	header('Location: .');
	
}

?>