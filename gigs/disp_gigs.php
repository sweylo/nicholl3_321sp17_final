<?php include('../view/header.php'); ?>

<h2>Gigs</h2>

<?php if ($me) { ?>

<span class="add"><a href="../gigs/?action=add_gig_form">Add gig</a></span>

<?php } ?>

<table class="display">
	
	<tr>
		<th>Gig name</th>
		<th>Venue name</th>
		<th>Gig date</th>
		<th>Gig payout</th>
		<?php if ($me) { ?>
		<?php if ($user_bands) { ?>
		<th width="1px"></th>
		<?php } ?>
		<th width="1px"></th>
		<th width="1px"></th>
		<?php } ?>
	</tr>

	<?php if ($gigs) { foreach ($gigs as $gig) { ?>

	<tr>
		<td><?php echo ($gig['gig_name']) ? $gig['gig_name'] : ' - '; ?></td>
		<td>
			<a href="../venues/?action=disp_venue_info&venue_id=<?php echo $gig['venue_id']; ?>">
				<?php echo $gig['venue_name']; ?>
			</a>
		</td>
		<td><?php echo date('M j, Y @ g:i a', $gig['gig_date']); ?></td>
		<td><?php echo '$' . $gig['gig_payout']; ?></td>
		<?php if ($me) { ?>
		<?php if ($user_bands && !$gig['is_booked']) { ?>
		<td>
			<a href="./?action=book_gig&gig_id=<?php echo $gig['gig_id']; ?>" 
				class="edit">Book</a>
		</td>
		<?php } else { ?>
		<td><!-- unbook --></td>
		<?php } ?>
		<td>
			<a href="./?action=edit_gig&gig_id=<?php echo $gig['gig_id']; ?>" 
				class="edit">Edit</a>
		</td>
		<td>
			<a href="./?action=delete_gig&gig_id=<?php echo $gig['gig_id']; ?>" 
				class="delete">Delete</a>
		</td>
		<?php } ?>
	</tr>

	<?php }} else { ?>
	
	<tr><td colspan="6">There are no gigs in the database.</td></tr>
		
	<?php } ?>
	
</table>

<?php include('../view/footer.php'); ?>