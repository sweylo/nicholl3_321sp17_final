<?php include('../view/header.php'); ?>

<h2>Add Gig</h2>

<?php if ($me) { ?>

<form action="./" method="post" class="std-form">
	
	<input type="hidden" name="action" value="save_gig">
	<input type="hidden" name="gig_id" value="<?php echo $gig_id; ?>">
	
	<h3>Venue</h3>
	<select name="gig_venue_id">
		<?php
		
		foreach ($user_venues as $user_venue) {
			$sel = ($user_venue['venue_id'] == $gig['gig_venue_id']) ? 'selected' : '';
			echo "<option value='$user_venue[venue_id]' $sel>$user_venue[venue_name]</option>\n";
		}
		
		?>
	</select>
	
	<h3>Info</h3>
	<input type="text" name="gig_name" placeholder="Gig name (optional)"
		value="<?php echo $gig['gig_name']; ?>"><br />
	<textarea rows="5" name="gig_description" placeholder="Description (optional)"
		><?php echo $gig['gig_description']; ?></textarea>
	
	<h3>Payout</h3>
	<input type="number" step="any" min="1" name="gig_payout" placeholder="Payout ($)"
		value="<?php echo $gig['gig_payout']; ?>"><br />
	
	<h3>Time &  Date</h3>
	
	<label>Date</label>
	<select name="gig_month" class="small">
		<?php
		
		foreach ($months as $k => $month) {
			$k++;
			$sel = ($k == $gig_month) ? 'selected' : '';
			echo "<option value='$k' $sel>$month</option>";
		}
		
		?>
	</select>
	<select name="gig_day" class="small">
		<?php
		
		for ($i = 1; $i <= 31; $i++) {
			$sel = ($i == $gig_day) ? 'selected' : '';
			echo "<option value='$i' $sel>$i</option>";
		}
		
		?>
	</select>
	<select name="gig_year" class="small">
		<?php
		
		for ($i = date('Y'); $i <= date('Y') + 10; $i++) {
			$sel = ($i == $gig_year) ? 'selected' : '';
			echo "<option value='$i' $sel>$i</option>";
		}
		
		?>
	</select>
	
	<label>Time</label>
	<select name="gig_hour" class="small">
		<?php
		
		for ($hour = 0; $hour < 24; $hour++) {
			$sel = ($hour == $gig_hour) ? 'selected' : '';
			$hour_12 = ($hour % 12 == 0) ? 12 : $hour % 12;
			$am_pm = ($hour < 12) ? 'AM' : 'PM';
			echo "<option value='$hour' $sel>$hour / $hour_12 $am_pm</option>";
		}
		
		?>
	</select>
	<select name="gig_minute" class="small">
		<?php
		
		for ($min = 0; $min < 60; $min++) {
			$min = ($min < 10) ? "0$min" : $min;
			$sel = ($min == $gig_minute) ? 'selected' : '';
			echo "<option value='$min' $sel>$min</option>";
		}
		
		?>
	</select>
	
	<div><input type="submit" value="Save"></div>
	
</form>

<?php } else { ?>

<p>You must be logged in to add a gig.</p>

<?php 

}

include('../view/footer.php'); 

?>