<?php include('../view/header.php'); ?>

<h2>Add Gig</h2>

<?php if ($me) { ?>

<form action="./" method="post" class="std-form">
	
	<input type="hidden" name="action" value="add_gig">
	
	<h3>Venue</h3>
	<select name="gig_venue_id">
		<?php
		
		foreach ($user_venues as $user_venue) {
			echo "<option value='$user_venue[venue_id]'>$user_venue[venue_name]</option>\n";
		}
		
		?>
	</select>
	
	<h3>Info</h3>
	<input type="text" name="gig_name" placeholder="Gig name (optional)"><br />
	<textarea rows="5" name="gig_description" placeholder="Description (optional)"></textarea>
	
	<h3>Payout</h3>
	<input type="number" step="any" min="1" name="gig_payout" placeholder="Payout ($)"><br />
	
	<h3>Time &  Date</h3>
	
	<label>Date</label>
	<select name="gig_month" class="small">
		<?php
		
		foreach ($months as $k => $month) {
			$k++;
			echo "<option value='$k'>$month</option>";
		}
		
		?>
	</select>
	<select name="gig_day" class="small">
		<?php
		
		for ($i = 1; $i <= 31; $i++) {
			echo "<option value='$i'>$i</option>";
		}
		
		?>
	</select>
	<select name="gig_year" class="small">
		<?php
		
		for ($i = date('Y'); $i <= date('Y') + 10; $i++) {
			echo "<option value='$i'>$i</option>";
		}
		
		?>
	</select>
	
	<label>Time</label>
	<select name="gig_hour" class="small">
		<?php
		
		for ($hour = 0; $hour < 24; $hour++) {
			$hour_12 = ($hour % 12 == 0) ? 12 : $hour % 12;
			$am_pm = ($hour < 12) ? 'AM' : 'PM';
			echo "<option value='$hour'>$hour / $hour_12 $am_pm</option>";
		}
		
		?>
	</select>
	<select name="gig_minute" class="small">
		<?php
		
		for ($min = 0; $min < 60; $min++) {
			$min = ($min < 10) ? "0$min" : $min;
			echo "<option value='$min'>$min</option>";
		}
		
		?>
	</select>
	
	<div><input type="submit" value="Add"></div>
	
</form>

<?php } else { ?>

<p>You must be logged in to add a gig.</p>

<?php 

}

include('../view/footer.php'); 

?>