-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema gig_searcher
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `gig_searcher` ;

-- -----------------------------------------------------
-- Schema gig_searcher
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `gig_searcher` DEFAULT CHARACTER SET utf8 ;
USE `gig_searcher` ;

-- -----------------------------------------------------
-- Table `gig_searcher`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gig_searcher`.`users` ;

CREATE TABLE IF NOT EXISTS `gig_searcher`.`users` (
  `user_id` INT NOT NULL AUTO_INCREMENT,
  `user_name` VARCHAR(128) NOT NULL,
  `user_password` VARCHAR(128) NOT NULL,
  `user_email` VARCHAR(128) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE INDEX `user_id_UNIQUE` (`user_id` ASC),
  UNIQUE INDEX `user_name_UNIQUE` (`user_name` ASC),
  UNIQUE INDEX `user_email_UNIQUE` (`user_email` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `gig_searcher`.`us_states`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gig_searcher`.`us_states` ;

CREATE TABLE IF NOT EXISTS `gig_searcher`.`us_states` (
  `us_state_code` VARCHAR(2) NOT NULL,
  `us_state_name` VARCHAR(45) NOT NULL,
  UNIQUE INDEX `us_state_name_UNIQUE` (`us_state_name` ASC),
  UNIQUE INDEX `us_state_code_UNIQUE` (`us_state_code` ASC),
  PRIMARY KEY (`us_state_code`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `gig_searcher`.`venues`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gig_searcher`.`venues` ;

CREATE TABLE IF NOT EXISTS `gig_searcher`.`venues` (
  `venue_id` INT NOT NULL AUTO_INCREMENT,
  `venue_name` VARCHAR(128) NOT NULL,
  `venue_address_1` VARCHAR(128) NOT NULL,
  `venue_address_2` VARCHAR(128) NOT NULL,
  `venue_city` INT NOT NULL,
  `venue_state_code` VARCHAR(2) NOT NULL,
  `venue_zip` VARCHAR(128) NOT NULL,
  `venue_description` LONGTEXT NULL,
  PRIMARY KEY (`venue_id`),
  UNIQUE INDEX `venue_id_UNIQUE` (`venue_id` ASC),
  UNIQUE INDEX `venue_name_UNIQUE` (`venue_name` ASC),
  INDEX `state_code_idx` (`venue_state_code` ASC),
  CONSTRAINT `state_code`
    FOREIGN KEY (`venue_state_code`)
    REFERENCES `gig_searcher`.`us_states` (`us_state_code`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `gig_searcher`.`bands`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gig_searcher`.`bands` ;

CREATE TABLE IF NOT EXISTS `gig_searcher`.`bands` (
  `band_id` INT NOT NULL AUTO_INCREMENT,
  `band_name` VARCHAR(128) NOT NULL,
  `band_description` LONGTEXT NULL,
  `band_zip` INT NOT NULL,
  PRIMARY KEY (`band_id`),
  UNIQUE INDEX `band_name_UNIQUE` (`band_name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `gig_searcher`.`gigs`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gig_searcher`.`gigs` ;

CREATE TABLE IF NOT EXISTS `gig_searcher`.`gigs` (
  `gig_id` INT NOT NULL AUTO_INCREMENT,
  `gig_name` VARCHAR(128) NULL,
  `gig_date` DATETIME NOT NULL,
  `gig_payout` DECIMAL(10,2) NOT NULL,
  `gig_venue_id` INT NOT NULL,
  `gig_description` LONGTEXT NULL,
  PRIMARY KEY (`gig_id`),
  UNIQUE INDEX `gig_id_UNIQUE` (`gig_id` ASC),
  UNIQUE INDEX `gig_name_UNIQUE` (`gig_name` ASC),
  INDEX `vendor_id_idx` (`gig_venue_id` ASC),
  CONSTRAINT `gig_venue_id`
    FOREIGN KEY (`gig_venue_id`)
    REFERENCES `gig_searcher`.`venues` (`venue_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `gig_searcher`.`events`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gig_searcher`.`events` ;

CREATE TABLE IF NOT EXISTS `gig_searcher`.`events` (
  `event_id` INT NOT NULL AUTO_INCREMENT,
  `gig_id` INT NOT NULL,
  `band_id` INT NOT NULL,
  `event_description` LONGTEXT NULL,
  PRIMARY KEY (`event_id`),
  UNIQUE INDEX `users_gigs_venues_id_UNIQUE` (`event_id` ASC),
  INDEX `gig_id_idx` (`gig_id` ASC),
  INDEX `band_id_idx` (`band_id` ASC),
  CONSTRAINT `event_band_id`
    FOREIGN KEY (`band_id`)
    REFERENCES `gig_searcher`.`bands` (`band_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `event_gig_id`
    FOREIGN KEY (`gig_id`)
    REFERENCES `gig_searcher`.`gigs` (`gig_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `gig_searcher`.`band_permissions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gig_searcher`.`band_permissions` ;

CREATE TABLE IF NOT EXISTS `gig_searcher`.`band_permissions` (
  `band_permission_id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `band_id` INT NOT NULL,
  `permission_level` INT NOT NULL DEFAULT 1,
  PRIMARY KEY (`band_permission_id`),
  INDEX `user_id_idx` (`user_id` ASC),
  INDEX `band_id_idx` (`band_id` ASC),
  CONSTRAINT `perm_band_user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `gig_searcher`.`users` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `perm_band_id`
    FOREIGN KEY (`band_id`)
    REFERENCES `gig_searcher`.`bands` (`band_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `gig_searcher`.`venue_permissions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gig_searcher`.`venue_permissions` ;

CREATE TABLE IF NOT EXISTS `gig_searcher`.`venue_permissions` (
  `venue_permission_id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `venue_id` INT NOT NULL,
  `permission_level` INT NOT NULL DEFAULT 1,
  PRIMARY KEY (`venue_permission_id`),
  INDEX `user_id_idx` (`user_id` ASC),
  INDEX `venue_id_idx` (`venue_id` ASC),
  CONSTRAINT `perm_venue_user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `gig_searcher`.`users` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `perm_venue_id`
    FOREIGN KEY (`venue_id`)
    REFERENCES `gig_searcher`.`venues` (`venue_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SET SQL_MODE = '';
GRANT USAGE ON *.* TO gig_searcher_user;
 DROP USER gig_searcher_user;
SET SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
CREATE USER 'gig_searcher_user' IDENTIFIED BY 'theycallmetheseeker';

GRANT ALL ON `gig_searcher`.* TO 'gig_searcher_user';

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
