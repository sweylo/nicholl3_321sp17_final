-- MySQL dump 10.13  Distrib 5.7.17, for Linux (x86_64)
--
-- Host: localhost    Database: gig_searcher
-- ------------------------------------------------------
-- Server version	5.7.17-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `band_permissions`
--

DROP TABLE IF EXISTS `band_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `band_permissions` (
  `band_permission_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `band_id` int(11) NOT NULL,
  `permission_level` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`band_permission_id`),
  KEY `user_id_idx` (`user_id`),
  KEY `band_id_idx` (`band_id`),
  CONSTRAINT `perm_band_id` FOREIGN KEY (`band_id`) REFERENCES `bands` (`band_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `perm_band_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `band_permissions`
--

LOCK TABLES `band_permissions` WRITE;
/*!40000 ALTER TABLE `band_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `band_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bands`
--

DROP TABLE IF EXISTS `bands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bands` (
  `band_id` int(11) NOT NULL AUTO_INCREMENT,
  `band_name` varchar(128) NOT NULL,
  `band_description` longtext,
  `band_zip` int(11) NOT NULL,
  PRIMARY KEY (`band_id`),
  UNIQUE KEY `band_name_UNIQUE` (`band_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bands`
--

LOCK TABLES `bands` WRITE;
/*!40000 ALTER TABLE `bands` DISABLE KEYS */;
/*!40000 ALTER TABLE `bands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `gig_id` int(11) NOT NULL,
  `band_id` int(11) NOT NULL,
  `event_description` longtext,
  PRIMARY KEY (`event_id`),
  UNIQUE KEY `users_gigs_venues_id_UNIQUE` (`event_id`),
  KEY `gig_id_idx` (`gig_id`),
  KEY `band_id_idx` (`band_id`),
  CONSTRAINT `event_band_id` FOREIGN KEY (`band_id`) REFERENCES `bands` (`band_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `event_gig_id` FOREIGN KEY (`gig_id`) REFERENCES `gigs` (`gig_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gigs`
--

DROP TABLE IF EXISTS `gigs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gigs` (
  `gig_id` int(11) NOT NULL AUTO_INCREMENT,
  `gig_name` varchar(128) DEFAULT NULL,
  `gig_date` datetime NOT NULL,
  `gig_payout` decimal(10,2) NOT NULL,
  `gig_venue_id` int(11) NOT NULL,
  `gig_description` longtext,
  PRIMARY KEY (`gig_id`),
  UNIQUE KEY `gig_id_UNIQUE` (`gig_id`),
  UNIQUE KEY `gig_name_UNIQUE` (`gig_name`),
  KEY `vendor_id_idx` (`gig_venue_id`),
  CONSTRAINT `gig_venue_id` FOREIGN KEY (`gig_venue_id`) REFERENCES `venues` (`venue_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gigs`
--

LOCK TABLES `gigs` WRITE;
/*!40000 ALTER TABLE `gigs` DISABLE KEYS */;
/*!40000 ALTER TABLE `gigs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `us_states`
--

DROP TABLE IF EXISTS `us_states`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `us_states` (
  `us_state_code` varchar(2) NOT NULL,
  `us_state_name` varchar(128) NOT NULL,
  PRIMARY KEY (`us_state_code`),
  UNIQUE KEY `us_state_name_UNIQUE` (`us_state_name`),
  UNIQUE KEY `us_state_code_UNIQUE` (`us_state_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `us_states`
--

LOCK TABLES `us_states` WRITE;
/*!40000 ALTER TABLE `us_states` DISABLE KEYS */;
INSERT INTO `us_states` VALUES ('AL','Alabama'),('AK','Alaska'),('AZ','Arizona'),('AR','Arkansas'),('CA','California'),('CO','Colorodo'),('CT','Connecticut'),('DE','Delaware'),('DC','District of Columbia'),('FM','Federated States of Micronesia'),('FL','Florida'),('GA','Georgia'),('GU','Guam'),('HI','Hawaii'),('ID','Idaho'),('IL','Illinois'),('IN','Indiana'),('IA','Iowa'),('KS','Kansas'),('KY','Kentucky'),('LA','Louisiana'),('ME','Maine'),('MH','Marshall Islands'),('MD','Maryland'),('MA','Massachusetts'),('MI','Michigan'),('MN','Minnesota'),('MS','Mississippi'),('MO','Missouri'),('MT','Montana'),('NE','Nebraska'),('NV','Nevada'),('NH','New Hampshire'),('NJ','New Jersey'),('NM','New Mexico'),('NY','New York'),('NC','North Carolina'),('ND','North Dakota'),('MP','Northern Mariana Islands'),('OH','Ohio'),('OK','Oklahoma'),('OR','Oregon'),('PW','Palau'),('PA','Pennsylvania'),('PR','Puerto Rico'),('RI','Rhode Island'),('SC','South Carolina'),('SD','South Dakota'),('TN','Tennessee'),('TX','Texas'),('UT','Utah'),('VT','Vermont'),('VI','Virgin Islands'),('VA','Virginia'),('WA','Washington'),('WV','West Virginia'),('WI','Wisconsin'),('WY','Wyoming');
/*!40000 ALTER TABLE `us_states` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(128) NOT NULL,
  `user_password` varchar(128) NOT NULL,
  `user_email` varchar(128) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`),
  UNIQUE KEY `user_name_UNIQUE` (`user_name`),
  UNIQUE KEY `user_email_UNIQUE` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'sweylo','798ac95134fd585bb8e129f65424372fee149e4e','nichols.logan@gmail.com');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venue_permissions`
--

DROP TABLE IF EXISTS `venue_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venue_permissions` (
  `venue_permission_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `permission_level` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`venue_permission_id`),
  KEY `user_id_idx` (`user_id`),
  KEY `venue_id_idx` (`venue_id`),
  CONSTRAINT `perm_venue_id` FOREIGN KEY (`venue_id`) REFERENCES `venues` (`venue_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `perm_venue_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venue_permissions`
--

LOCK TABLES `venue_permissions` WRITE;
/*!40000 ALTER TABLE `venue_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `venue_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venues`
--

DROP TABLE IF EXISTS `venues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venues` (
  `venue_id` int(11) NOT NULL AUTO_INCREMENT,
  `venue_name` varchar(128) NOT NULL,
  `venue_address_1` varchar(128) NOT NULL,
  `venue_address_2` varchar(128) NOT NULL,
  `venue_city` int(11) NOT NULL,
  `venue_state_code` varchar(2) NOT NULL,
  `venue_zip` varchar(128) NOT NULL,
  `venue_description` longtext,
  PRIMARY KEY (`venue_id`),
  UNIQUE KEY `venue_id_UNIQUE` (`venue_id`),
  UNIQUE KEY `venue_name_UNIQUE` (`venue_name`),
  KEY `state_code_idx` (`venue_state_code`),
  CONSTRAINT `state_code` FOREIGN KEY (`venue_state_code`) REFERENCES `us_states` (`us_state_code`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venues`
--

LOCK TABLES `venues` WRITE;
/*!40000 ALTER TABLE `venues` DISABLE KEYS */;
/*!40000 ALTER TABLE `venues` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-04-13 12:08:10
