<?php include('../view/header.php'); ?>

<h2>Add Band</h2>

<?php if ($me) { ?>

<form action="./" method="post" class="std-form">
	
	<input type="hidden" name="action" value="add_band">
	
	<h3>Bio</h3>
	<input type="text" name="band_name" placeholder="Band name"><br />
	<textarea rows="5" name="band_description" placeholder="Description (optional)"></textarea>
	
	<h3>Location</h3>
	<input type="number" max="99999" name="band_zip" placeholder="Zip code"><br />
	
	<div><input type="submit" value="Add"></div>
	
</form>

<?php } else { ?>

<p>You must be logged in to add a band.</p>

<?php 

}

include('../view/footer.php'); 

?>