<?php include('../view/header.php'); ?>

<h2>Edit Band</h2>

<?php if ($me) { ?>

<form action="./" method="post" class="std-form">
	
	<input type="hidden" name="action" value="save_band">
	<input type="hidden" name="band_id" value="<?php echo $band_id; ?>">
	
	<h3>Bio</h3>
	<input type="text" name="band_name" placeholder="Band name"
		value="<?php echo $band['band_name']; ?>"><br />
	<textarea rows="5" name="band_description" placeholder="Description (optional)"
		><?php echo $band['band_description']; ?></textarea>
	
	<h3>Location</h3>
	<input type="number" max="99999" name="band_zip" placeholder="Zip code"
		value="<?php echo $band['band_zip']; ?>"><br />
	
	<div><input type="submit" value="Save"></div>
	
</form>

<?php } else { ?>

<p>You must be logged in to add a band.</p>

<?php 

}

include('../view/footer.php'); 

?>