<?php include('../view/header.php'); ?>

<h2>Bands</h2>

<?php if ($me) { ?>

<span class="add"><a href="../bands/?action=add_band_form">Add band</a></span>

<?php } ?>

<table class="display">
	
	<tr>
		<th>Name</th>
		<?php if ($me) { ?>
		<th width="1px"></th>
		<th width="1px"></th>
		<?php } ?>
	</tr>

	<?php if ($bands) { foreach ($bands as $band) { ?>

	<tr>
		<td>
			<a href="./?action=disp_band_info&band_id=<?php echo $band['band_id']; ?>">
				<?php echo $band['band_name']; ?>
			</a>
		</td>
		<?php if ($me) { ?>
		<td>
			<a href="./?action=edit_band&band_id=<?php echo $band['band_id']; ?>" 
				class="edit">Edit</a>
		</td>
		<td>
			<a href="./?action=delete_band&band_id=<?php echo $band['band_id']; ?>" 
				class="delete">Delete</a>
		</td>
		<?php } ?>
	</tr>

	<?php }} else { ?>
	
	<tr><td colspan="3">There are no bands in the database.</td></tr>
		
	<?php } ?>
	
</table>

<?php include('../view/footer.php'); ?>