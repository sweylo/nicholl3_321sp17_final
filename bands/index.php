<?php

/**
 *	controller for bands
 */

$page = 'bands';

require_once('../model/db.php');
require_once('../model/user_db.php');
require_once('../model/band_db.php');
require_once('../model/us_state_db.php');

$action = filter_input(INPUT_POST, 'action');
if ($action === NULL) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action === NULL) {
		$action = 'disp_bands';
    }
}

if ($action == 'disp_bands') {
	
	$bands = get_bands();
	
	include('disp_bands.php');
	
} else if ($action == 'disp_band_info') {
	
	$band_id = filter_input(INPUT_GET, 'band_id', FILTER_VALIDATE_INT);
	
	if ($band_id == null || $band_id == false) {
		die('invalid band id');
	}
	
	$band = get_band_by_id($band_id);
	
	include('disp_band_info.php');
	
} else if ($action == 'add_band_form') {
	
	include('add_band.php');
	
} else if ($action == 'add_band') {
	
	$bandname = filter_input(INPUT_POST, 'band_name');
	$description = filter_input(INPUT_POST, 'band_description');
	$zip = filter_input(INPUT_POST, 'band_zip', FILTER_VALIDATE_INT);
	
	if ($bandname == null || $bandname == false || 
		$zip == null || $zip == false
	) {
		die('invalid data (probably something was left blank)');
	}
	
	if (!$me) {
		die('must be logged in to add bands');
	}
	
	add_band($me['user_id'], $bandname, $description, $zip);
	
	header('Location: .');
	
} else if ($action == 'edit_band') {

	$band_id = filter_input(INPUT_GET, 'band_id', FILTER_VALIDATE_INT);
	
	if ($band_id == null || $band_id == false) {
		die('invalid venue id');
	}
	
	$permission_level = get_band_permission_level($me['user_id'], $band_id);
	
	// check if the user is at least an admin of the venue
	if ($permission_level < 3) {
		$error = 'you don\'t have permission to edit this band';
		include('../view/error.php');
		die();
	}
	
	$band = get_band_by_id($band_id);
	
	include('edit_band.php');
	
} else if ($action == 'save_band') {
	
	$band_id = filter_input(INPUT_POST, 'band_id');
	
	if ($band_id == null || $band_id == false) {
		die('invalid band id');
	}
	
	$bandname = filter_input(INPUT_POST, 'band_name');
	$description = filter_input(INPUT_POST, 'band_description');
	$zip = filter_input(INPUT_POST, 'band_zip', FILTER_VALIDATE_INT);
	
	if ($bandname == null || $bandname == false || 
		$zip == null || $zip == false
	) {
		die('invalid data (probably something was left blank)');
	}
	
	if (!$me) {
		die('must be logged in to save bands');
	}
	
	save_band($band_id, $bandname, $description, $zip);
	
	header("Location: ./?action=disp_band_info&band_id=$band_id");
	
} else if ($action == 'delete_band') {
	
	$band_id = filter_input(INPUT_GET, 'band_id', FILTER_VALIDATE_INT);
	
	if ($band_id == null || $band_id == false) {
		die('invalid band id');
	}
	
	$permission_level = get_band_permission_level($me['user_id'], $band_id);
	
	// check if the user is an admin of the band
	if ($permission_level < 3) {
		$error = 'you don\'t have permission to delete this band';
		include('../view/error.php');
		die();
	}
	
	delete_band($band_id);
	
	header('Location: .');
	
}

?>