<?php require('../view/header.php'); ?>

<h2>Registration</h2>

<form action="./" method="post" class="std-form">
	<input type="hidden" name="action" value="add_user">
	<div><input type="text" name="user_name" placeholder="Username"></div>
	<span>&nbsp;</span>
	<div><input type="password" name="user_password" placeholder="Password"></div>
	<div><input type="password" name="confirm" placeholder="Confirm Password"></div>
	<span>&nbsp;</span>
	<div><input type="text" name="user_email" placeholder="Email address"></div>
	<span>&nbsp;</span>
	<div><input type="submit" value="Register"></div>
</form>

<?php require('../view/footer.php'); ?>