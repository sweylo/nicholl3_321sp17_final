<?php

/**
 *	controller for home/users
 */

session_start();

$page = 'home';

require_once('../model/db.php');
require_once('../model/user_db.php');
require_once('../model/band_db.php');
require_once('../model/venue_db.php');
require_once('../model/gig_db.php');
require_once('../model/event_db.php');

$action = filter_input(INPUT_POST, 'action');
if ($action === NULL) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action === NULL) {
		$action = 'disp_welcome';
    }
}

if ($action == 'login') {
	
	$username = filter_input(INPUT_POST, 'user_name');
	$password = filter_input(INPUT_POST, 'user_password');
	
	if ($username == null || $username == false || $password == null || $password == false) {
		$error = 'invalid username/password (probably left blank)';
		include('../view/error.php');
		die();
	}
	
	if (validate_user($username, $password)) {
		$_SESSION['user'] = $username;
		header("Location: $_SERVER[HTTP_REFERER]");
	} else {
		$error = 'incorrect username/password';
		include('../view/error.php');
		die();
	}
	
} else if ($action == 'logout') {
	
	session_unset();
	session_destroy();
	header("Location: $_SERVER[HTTP_REFERER]");
	
} else if ($action == 'register') {
	
	$register = true;
	
	// prevent a logged in user from accessing register page
	if ($me) {
		header('Location: .');
	} else {
		include('register.php');
	}
	
} else if ($action == 'add_user') {
	
	$username = filter_input(INPUT_POST, 'user_name');
	$password = filter_input(INPUT_POST, 'user_password');
	$confirm = filter_input(INPUT_POST, 'confirm');
	$email = filter_input(INPUT_POST, 'user_email', FILTER_VALIDATE_EMAIL);
	
	if (
		$username === null || $username === false || 
		$password === null || $password === false ||
		$confirm === null || $confirm === false ||
		$email === null || $email === false
	) {
		die('invalid data');
	} else if ($password != $confirm) {
		die('passwords did not match');
	}
	
	add_user($username, sha1($username . $password), $email);
	
	header('Location: .');
	
} else if ($action == 'search') {
	
	$query = filter_input(INPUT_GET, 'query');
	
	if ($query === null || $query === false) {
		die('invalid search query');
	}
	
	include('search_results.php');
	
} else if ($action == 'disp_welcome') {
	
	$events = get_upcoming_events();
	include('home.php');
	
}

?>