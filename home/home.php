<?php include('../view/header.php'); ?>

<h2>Welcome to Gig Searcher</h2>

<p>Choose from one of the options to the left, or login to manage your band/venue.</p>

<?php if ($events) { ?>

<h3>Upcoming events:</h3>

<table class="display">
	
	<tr>
		<th>Band name</th>
		<th>Venue name</th>
		<th>Event date</th>
	</tr>

	<?php foreach ($events as $event) { ?>

	<tr>
		<td>
			<a href="../bands/?action=disp_band_info&band_id=<?php echo $event['band_id']; ?>">
				<?php echo $event['band_name']; ?>
			</a>
		</td>
		<td>
			<a href="../venues/?action=disp_venue_info&venue_id=<?php echo $event['venue_id']; ?>">
				<?php echo $event['venue_name']; ?>
			</a>
		</td>
		<td><?php echo date('M j, Y @ g:i a', $event['gig_date']); ?></td>
	</tr>

	<?php }} ?>
	
</table>

<?php include('../view/footer.php'); ?>