<?php include('../view/header.php'); ?>

<h2>Venues</h2>

<?php if ($me) { ?>

<span class="add"><a href="../venues/?action=add_venue_form">Add venue</a></span>

<?php } ?>

<table class="display">
	
	<tr>
		<th>Name</th>
		<?php if ($me) { ?>
		<th width="1px"></th>
		<th width="1px"></th>
		<?php } ?>
	</tr>

	<?php if ($venues) { foreach ($venues as $venue) { ?>

	<tr>
		<td>
			<a href="./?action=disp_venue_info&venue_id=<?php echo $venue['venue_id']; ?>">
				<?php echo $venue['venue_name']; ?>
			</a>
		</td>
		<?php if ($me) { ?>
		<td>
			<a href="./?action=edit_venue&venue_id=<?php echo $venue['venue_id']; ?>" 
				class="edit">Edit</a>
		</td>
		<td>
			<a href="./?action=delete_venue&venue_id=<?php echo $venue['venue_id']; ?>" 
				class="delete">Delete</a>
		</td>
		<?php } ?>
	</tr>

	<?php }} else { ?>
	
	<tr><td colspan="3">There are no venues in the database.</td></tr>
		
	<?php } ?>
	
</table>

<?php include('../view/footer.php'); ?>