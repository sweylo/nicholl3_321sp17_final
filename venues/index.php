<?php

/**
 *	controller for venues
 */

$page = 'venues';

require_once('../model/db.php');
require_once('../model/user_db.php');
require_once('../model/venue_db.php');
require_once('../model/us_state_db.php');

$action = filter_input(INPUT_POST, 'action');
if ($action === NULL) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action === NULL) {
		$action = 'disp_venues';
    }
}

if ($action == 'disp_venues') {
	
	$venues = get_venues();
	
	include('disp_venues.php');
	
} else if ($action == 'disp_venue_info') {
	
	$venue_id = filter_input(INPUT_GET, 'venue_id', FILTER_VALIDATE_INT);
	
	if ($venue_id == null || $venue_id == false) {
		die('invalid venue id');
	}
	
	$venue = get_venue_by_id($venue_id);
	
	include('disp_venue_info.php');
	
} else if ($action == 'add_venue_form') {
	
	$states = get_states();
	
	include('add_venue.php');
	
} else if ($action == 'add_venue') {
	
	$venuename = filter_input(INPUT_POST, 'venue_name');
	$description = filter_input(INPUT_POST, 'venue_description');
	$address_1 = filter_input(INPUT_POST, 'venue_address_1');
	$address_2 = filter_input(INPUT_POST, 'venue_address_2');
	$city = filter_input(INPUT_POST, 'venue_city');
	$state = filter_input(INPUT_POST, 'venue_state');
	$zip = filter_input(INPUT_POST, 'venue_zip', FILTER_VALIDATE_INT);
	
	if ($venuename == null || $venuename == false ||
		$address_1 == null || $address_1 == false ||
		$city == null || $city == false ||
		$state == null || $state == false ||
		$zip == null || $zip == false
	) {
		die('invalid data (something may have been left blank)');
	}
	
	if (!$me) {
		die('must be logged in to add venues');
	}
	
	add_venue($me['user_id'], $venuename, $description, $address_1, $address_2, $city, $state, $zip);
	
	header('Location: .');
	
} else if ($action == 'edit_venue') {
	
	$states = get_states();
	$venue_id = filter_input(INPUT_GET, 'venue_id', FILTER_VALIDATE_INT);
	
	if ($venue_id == null || $venue_id == false) {
		die('invalid venue id');
	}
	
	$permission_level = get_venue_permission_level($me['user_id'], $venue_id);
	
	// check if the user is at least an admin of the venue
	if ($permission_level < 3) {
		$error = 'you don\'t have permission to edit this venue';
		include('../view/error.php');
		die();
	}
	
	$venue = get_venue_by_id($venue_id);
	
	include('edit_venue.php');
	
} else if ($action == 'save_venue') {
	
	$venue_id = filter_input(INPUT_POST, 'venue_id', FILTER_VALIDATE_INT);
	
	if ($venue_id == null || $venue_id == false) {
		die('invalid venue id');
	}
	
	$venuename = filter_input(INPUT_POST, 'venue_name');
	$description = filter_input(INPUT_POST, 'venue_description');
	$address_1 = filter_input(INPUT_POST, 'venue_address_1');
	$address_2 = filter_input(INPUT_POST, 'venue_address_2');
	$city = filter_input(INPUT_POST, 'venue_city');
	$state = filter_input(INPUT_POST, 'venue_state');
	$zip = filter_input(INPUT_POST, 'venue_zip', FILTER_VALIDATE_INT);
	
	if ($venuename == null || $venuename == false ||
		$address_1 == null || $address_1 == false ||
		$city == null || $city == false ||
		$state == null || $state == false ||
		$zip == null || $zip == false
	) {
		die('invalid data (something may have been left blank)');
	}
	
	if (!$me) {
		die('must be logged in to save venues');
	}
	
	save_venue($venue_id, $venuename, $description, $address_1, $address_2, $city, $state, $zip);
	
	header("Location: ./?action=disp_venue_info&venue_id=$venue_id");
	
} else if ($action == 'delete_venue') {
	
	$venue_id = filter_input(INPUT_GET, 'venue_id', FILTER_VALIDATE_INT);
	
	if ($venue_id == null || $venue_id == false) {
		die('invalid venue id');
	}
	
	$permission_level = get_venue_permission_level($me['user_id'], $venue_id);
	
	// check if the user is an admin of the venue (admins and owners have permission to delete)
	if ($permission_level < 3) {
		$error = 'you don\'t have permission to delete this venue';
		include('../view/error.php');
		die();
	}
	
	delete_venue($venue_id);
	
	header('Location: .');
	
}

?>