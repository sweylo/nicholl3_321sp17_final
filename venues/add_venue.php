<?php include('../view/header.php'); ?>

<h2>Add Venue</h2>

<?php if ($me) { ?>

<form action="./" method="post" class="std-form">
	
	<input type="hidden" name="action" value="add_venue">
	
	<h3>Info</h3>
	
	<input type="text" name="venue_name" placeholder="Venu name (optional)"><br />
	<textarea rows="5" name="venue_description" placeholder="Description (optional)"></textarea>
	
	<h3>Location</h3>
	<input type="text" name="venue_address_1" placeholder="Address 1"><br />
	<input type="text" name="venue_address_2" placeholder="Address 2 (optional)"><br />
	<input type="text" name="venue_city" placeholder="City"><br />
	<!--<label>State</label>-->
	<select name="venue_state">
		<?php
		
		foreach ($states as $state) {
			echo "<option value='$state[us_state_code]'>$state[us_state_name]</option>\n";
		}
		
		?>
	</select>
	<input type="number" max="99999" name="venue_zip" placeholder="Zip code"><br />
	
	<div><input type="submit" value="Add"></div>
	
</form>

<?php } else { ?>

<p>You must be logged in to add a venue.</p>

<?php 

}

include('../view/footer.php'); 

?>