<?php include('../view/header.php'); ?>

<h2>Edit Venue</h2>

<?php if ($me) { ?>

<form action="./" method="post" class="std-form">
	
	<input type="hidden" name="action" value="save_venue">
	<input type="hidden" name="venue_id" value="<?php echo $venue_id; ?>">
	
	<h3>Info</h3>
	
	<input type="text" name="venue_name" placeholder="Venu name (optional)" 
		   value="<?php echo $venue['venue_name']; ?>"><br />
	<textarea rows="5" name="venue_description" placeholder="Description (optional)"
		><?php echo $venue['venue_description']; ?></textarea>
	
	<h3>Location</h3>
	<input type="text" name="venue_address_1" placeholder="Address 1"
		value="<?php echo $venue['venue_address_1']; ?>"><br />
	<input type="text" name="venue_address_2" placeholder="Address 2 (optional)"
		value="<?php echo $venue['venue_address_2']; ?>"><br />
	<input type="text" name="venue_city" placeholder="City"
		value="<?php echo $venue['venue_city']; ?>"><br />
	<!--<label>State</label>-->
	<select name="venue_state">
		<?php
		
		foreach ($states as $state) {
			$sel = ($state['us_state_code'] == $venue['venue_state_code']) ? 'selected' : '';
			echo "<option value='$state[us_state_code]' $sel>$state[us_state_name]</option>\n";
		}
		
		?>
	</select>
	<input type="number" max="99999" name="venue_zip" placeholder="Zip code"
		value="<?php echo $venue['venue_zip']; ?>"><br />
	
	<div><input type="submit" value="Save"></div>
	
</form>

<?php } else { ?>

<p>You must be logged in to edit a venue.</p>

<?php 

}

include('../view/footer.php'); 

?>