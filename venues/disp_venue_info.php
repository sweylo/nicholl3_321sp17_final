<?php include('../view/header.php'); ?>

<h2><?php echo $venue['venue_name']; ?></h2>

<h3>Description</h3>
<p><?php echo $venue['venue_description']; ?></p>

<h3>Location</h3>
<p>
	<?php 
	
	echo $venue['venue_address_1'];
	echo ($venue['venue_address_2']) ? '<br />' . $venue['venue_address_2'] : ''; 
	echo '<br />' . $venue['venue_city'] 
		. ', ' . $venue['venue_state_code'] 
		. ', ' . $venue['venue_zip'];
	
	?>
</p>
<p>

<?php include('../view/footer.php'); ?>