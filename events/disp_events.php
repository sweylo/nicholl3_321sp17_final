<?php include('../view/header.php'); ?>

<h2>Events</h2>

<table class="display">
	
	<tr>
		<th>Band</th>
		<th>Venue</th>
		<th>Date</th>
	</tr>

	<?php if ($events) { foreach ($events as $event) { ?>

	<tr>
		<td>
			<a href="../bands/?action=disp_band_info&band_id=<?php echo $event['band_id']; ?>">
				<?php echo $event['band_name']; ?>
			</a>
		</td>
		<td>
			<a href="../venues/?action=disp_venue_info&venue_id=<?php echo $event['venue_id']; ?>">
				<?php echo $event['venue_name']; ?>
			</a>
		</td>
		<td><?php echo date('M j, Y @ g:i a', $event['gig_date']); ?></td>
	</tr>

	<?php }} else { ?>
	
	<tr><td colspan="4">There are no events in the database.</td></tr>
		
	<?php } ?>
	
</table>

<?php include('../view/footer.php'); ?>