<?php

/**
 *	controller for events
 */

$page = 'events';

require_once('../model/db.php');
require_once('../model/user_db.php');
require_once('../model/band_db.php');
require_once('../model/gig_db.php');
require_once('../model/event_db.php');

$action = filter_input(INPUT_POST, 'action');
if ($action === NULL) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action === NULL) {
		$action = 'disp_events';
    }
}

if ($action == 'disp_events') {
	
	$events = get_events();
	include('disp_events.php');
	
}

?>