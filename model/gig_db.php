<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set("display_errors", 1);
date_default_timezone_set('UTC');

function get_gigs() {
	
	global $db;
	
	$sql = 'SELECT *
			FROM gigs JOIN venues
				ON gig_venue_id = venue_id
			ORDER BY gig_name';
	
	$statement = $db -> prepare($sql);
	$statement -> execute();
	$gigs = $statement -> fetchAll();
	$statement -> closeCursor();
	
	return $gigs;
	
}

function get_gig_by_id($gig_id) {
	
	global $db;
	
	$sql = 'SELECT *
			FROM gigs JOIN venues
				ON gig_venue_id = venue_id
			WHERE gig_id = :gig_id';
	
	$statement = $db -> prepare($sql);
	$statement -> bindValue(':gig_id', $gig_id);
	$statement -> execute();
	$gigs = $statement -> fetch();
	$statement -> closeCursor();
	
	return $gigs;
	
}

// should really be in event_db
function book_gig($gig_id, $band_id) {
	
	global $db;
	
	// add the record to the gigs table
	$sql = "INSERT INTO events 
			VALUES (default, :gig_id, :band_id, default)";
	
	$statement = $db -> prepare($sql);
	$statement -> bindValue(':gig_id', $gig_id);
	$statement -> bindValue(':band_id', $band_id);
	$statement -> execute();
	$statement -> closeCursor();
	
}

function is_gig_booked($gig_id) {
	
	global $db;
	
	$sql = 'SELECT *
			FROM gigs g JOIN events e
				ON g.gig_id = e.gig_id
			WHERE g.gig_id = :gig_id';
	
	$statement = $db -> prepare($sql);
	$statement -> bindValue(':gig_id', $gig_id);
	$statement -> execute();
	$bookings = $statement -> fetchAll();
	$statement -> closeCursor();
	
	return $bookings && count($bookings) > 0;
	
}

function add_gig($venue_id, $gigname, $description, $payout, $timestamp) {
	
	global $db;
	
	// add the record to the gigs table
	$sql = "INSERT INTO gigs 
			VALUES (default, :gig_name, :gig_date, :gig_payout, :gig_venue_id, :gig_description)";
	
	$statement = $db -> prepare($sql);
	$statement -> bindValue(':gig_name', $gigname);
	$statement -> bindValue(':gig_description', $description);
	$statement -> bindValue(':gig_date', $timestamp);
	$statement -> bindValue(':gig_payout', $payout);
	$statement -> bindValue(':gig_venue_id', $venue_id);
	$statement -> execute();
	echo $db -> errorInfo();
	$statement -> closeCursor();
	
}

function save_gig($gig_id, $venue_id, $gigname, $description, $payout, $timestamp) {
	
	global $db;
	
	$sql = 'UPDATE gigs 
			SET gig_name = :gig_name,
				gig_date = :gig_date,
				gig_payout = :gig_payout,
				gig_venue_id = :gig_venue_id,
				gig_description = :gig_description 
			WHERE gig_id = :gig_id';
	
	$statement = $db -> prepare($sql);
	$statement -> bindValue(':gig_id', $gig_id);
	$statement -> bindValue(':gig_name', $gigname);
	$statement -> bindValue(':gig_description', $description);
	$statement -> bindValue(':gig_date', $timestamp);
	$statement -> bindValue(':gig_payout', $payout);
	$statement -> bindValue(':gig_venue_id', $venue_id);
	$statement -> execute();
	
}

function delete_gig($gig_id) {
	
	global $db;

	// delete the record to the events table
	$sql = 'DELETE FROM events
			WHERE gig_id = :gig_id';
	
	$statement = $db -> prepare($sql);
	$statement -> bindValue(':gig_id', $gig_id);
	$statement -> execute();
	$statement -> closeCursor();
	
	// delete the record to the gigs table
	$sql = 'DELETE FROM gigs 
			WHERE gig_id = :gig_id';
	
	$statement = $db -> prepare($sql);
	$statement -> bindValue(':gig_id', $gig_id);
	$statement -> execute();
	$statement -> closeCursor();
	
}

?>