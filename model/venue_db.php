<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set("display_errors", 1);
date_default_timezone_set('UTC');

function get_venues() {
	
	global $db;
	
	$sql = 'SELECT *
			FROM venues
			ORDER BY venue_name';
	
	$statement = $db -> prepare($sql);
	$statement -> execute();
	$venues = $statement -> fetchAll();
	$statement -> closeCursor();
	
	return $venues;
	
}

function get_venue_by_id($venue_id) {
	
	global $db;
	
	$sql = 'SELECT *
			FROM venues
			WHERE venue_id = :venue_id';
	
	$statement = $db -> prepare($sql);
	$statement -> bindValue(':venue_id', $venue_id);
	$statement -> execute();
	$venue = $statement -> fetch();
	$statement -> closeCursor();
	
	return $venue;
	
}

// returns venues that the given user has at least a given permission level
function get_venues_by_user($user_id, $permission_level = 1) {
	
	global $db;
	
	$sql = 'SELECT * 
			FROM venues v JOIN venue_permissions vp 
				ON v.venue_id = vp.venue_id 
			WHERE vp.user_id = :user_id
				AND vp.permission_level >= :permission_level';
	
	$statement = $db -> prepare($sql);
	$statement -> bindValue(':user_id', $user_id);
	$statement -> bindValue(':permission_level', $permission_level);
	$statement -> execute();
	$user_venues = $statement -> fetchAll();
	$statement -> closeCursor();
	
	return $user_venues;
	
}

function add_venue($creator_user_id, $venuename, $description, $address_1, $address_2, $city, 
	$state, $zip) {
	
	global $db;
	
	// add the record to the venues table
	$sql = 'INSERT INTO venues 
			VALUES (default, :venue_name, :venue_address_1, :venue_address_2, :venue_city, 
				:venue_state_code, :venue_zip, :venue_description)';
	
	$statement = $db -> prepare($sql);
	$statement -> bindValue(':venue_name', $venuename);
	$statement -> bindValue(':venue_description', $description);
	$statement -> bindValue(':venue_address_1', $address_1);
	$statement -> bindValue(':venue_address_2', $address_2);
	$statement -> bindValue(':venue_city', $city);
	$statement -> bindValue(':venue_state_code', $state);
	$statement -> bindValue(':venue_zip', $zip);
	$statement -> execute();
	
	print_r($statement);
	
	// get the id of the newly created record
	$new_venue_id = $db -> lastInsertId();
	
	if (!$new_venue_id) {
		die('<br />unable to write new venue record');
	}
	
	// add a record in the permissions table giving ownership to the creating user
	$sql = 'INSERT INTO venue_permissions (user_id, venue_id, permission_level) 
			VALUES (:user_id, :venue_id, 4)';
	
	$statement = $db -> prepare($sql);
	$statement -> bindValue(':user_id', $creator_user_id);
	$statement -> bindValue(':venue_id', $new_venue_id);
	$statement -> execute();
	
	$statement -> closeCursor();
	
}

function save_venue($venue_id, $venuename, $description, $address_1, $address_2, $city, $state, 
	$zip) {
	
	global $db;
	
	$sql = 'UPDATE venues 
			SET venue_name = :venue_name,
				venue_address_1 = :venue_address_1,
				venue_address_2 = :venue_address_2,
				venue_city = :venue_city,
				venue_state_code = :venue_state_code,
				venue_zip = :venue_zip,
				venue_description = :venue_description
			WHERE venue_id = :venue_id';
	
	$statement = $db -> prepare($sql);
	$statement -> bindValue(':venue_id', $venue_id);
	$statement -> bindValue(':venue_name', $venuename);
	$statement -> bindValue(':venue_description', $description);
	$statement -> bindValue(':venue_address_1', $address_1);
	$statement -> bindValue(':venue_address_2', $address_2);
	$statement -> bindValue(':venue_city', $city);
	$statement -> bindValue(':venue_state_code', $state);
	$statement -> bindValue(':venue_zip', $zip);
	$statement -> execute();
	
}

function get_venue_permission_level($user_id, $venue_id) {
	
	global $db;
	
	// get the record matching the user and the venue
	$sql = "SELECT * 
			FROM venues v JOIN venue_permissions vp 
				ON v.venue_id = vp.venue_id 
			WHERE v.venue_id = :venue_id
				AND vp.user_id = :user_id";
	
	$statement = $db -> prepare($sql);
	$statement -> bindValue(':venue_id', $venue_id);
	$statement -> bindValue(':user_id', $user_id);
	$statement -> execute();
	$permission = $statement -> fetch();
	
	$statement -> closeCursor();
	
	return $permission['permission_level'];
	
}

function delete_venue($venue_id) {
	
	global $db;
	
	// delete any records from gigs table having this venue
	$sql = 'DELETE FROM gigs
			WHERE gig_venue_id = :venue_id';
	
	$statement = $db -> prepare($sql);
	$statement -> bindValue(':venue_id', $venue_id);
	$statement -> execute();
	$statement -> closeCursor();
	
	// delete any records from the venues permission table
	$sql = 'DELETE FROM venue_permissions
			WHERE venue_id = :venue_id';
	
	$statement = $db -> prepare($sql);
	$statement -> bindValue(':venue_id', $venue_id);
	$statement -> execute();
	$statement -> closeCursor();

	// delete the record from the venues table
	$sql = 'DELETE FROM venues 
			WHERE venue_id = :venue_id';
	
	$statement = $db -> prepare($sql);
	$statement -> bindValue(':venue_id', $venue_id);
	$statement -> execute();
	$statement -> closeCursor();
	
}

?>