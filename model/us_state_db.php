<?php

function get_states() {
	
	global $db;
	
	$sql = 'SELECT *
			FROM us_states
			ORDER BY us_state_name';
	
	$statement = $db -> prepare($sql);
	$statement -> execute();
	$states = $statement -> fetchAll();
	$statement -> closeCursor();
	
	return $states;
	
}

?>