<?php

session_start();

function get_users() {
	
	global $db;
	
	$sql = 'SELECT *
			FROM users
			ORDER BY user_name';
	
	$statement = $db -> prepare($sql);
	$statement -> execute();
	$users = $statement -> fetchAll();
	$statement -> closeCursor();
	
	return $users;
	
}

function get_user_by_name($username) {
	
	global $db;
	
	$sql = 'SELECT * 
			FROM users
			WHERE user_name = :username';
	
	$statement = $db -> prepare($sql);
	$statement -> bindValue(':username', $username, PDO::PARAM_STR);
	$statement -> execute();
	$user = $statement -> fetch(PDO::FETCH_ASSOC);
	$statement -> closeCursor();
	
	return $user;
	
}

function add_user($username, $password, $email) {
	
	global $db;
	
	$sql = 'INSERT INTO users (user_name, user_password, user_email) 
			VALUES (:user_name, :user_password, :user_email)';
	
	$statement = $db -> prepare($sql);
	$statement -> bindValue(':user_name', $username);
	$statement -> bindValue(':user_password', $password);
	$statement -> bindValue(':user_email', $email);
	$statement -> execute();
	$statement -> closeCursor();
	
}

function edit_user($user_id, $user_name, $user_password, $user_email) {
	
	global $db;
	
	$sql = 'UPDATE users
			SET user_name = :user_name, user_password :user_password, user_email :user_email
			WHERE user_id = :user_id';
	
	$statement = $db -> prepare($sql);
	$statement -> bindValue(':user_id', $user_id);
	$statement -> bindValue(':user_name', $user_name);
	$statement -> bindValue(':user_password', $user_password);
	$statement -> bindValue(':user_email', $user_email);
	$statement -> execute();
	$statement -> closeCursor();
	
}

function delete_user($user_id) {
	
	global $db;
	
	$sql = 'DELETE FROM users 
			WHERE user_id = :user_id';
	
	$statement = $db -> prepare($sql);
	$statement -> bindValue(':user_id', $user_id);
	$statement -> execute();
	$statement -> closeCursor();
	
}

function validate_user($username, $password) {
	$check_user = get_user_by_name($username);
	return sha1($username . $password) == $check_user['user_password'];
}

// simplified gravatar implementation from https://en.gravatar.com/site/implement/images/php/
function get_gravatar($email, $s = 16, $d = 'mm', $r = 'g') {
    $url = 'https://www.gravatar.com/avatar/';
    $url .= md5(strtolower(trim($email)));
    $url .= "?s=$s&d=$d&r=$r";
    return $url;
}

// check to see a user is logged in
$me = (isset($_SESSION['user'])) ? get_user_by_name($_SESSION['user']) : false;

?>