<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set("display_errors", 1);
date_default_timezone_set('UTC');

function get_events() {
	
	global $db;
	
	$sql = 'SELECT *
			FROM events e 
				JOIN gigs g
					ON e.gig_id = g.gig_id
				JOIN bands b
					ON e.band_id = b.band_id
				JOIN venues v
					ON g.gig_venue_id = v.venue_id
			ORDER BY gig_date';
	
	$statement = $db -> prepare($sql);
	$statement -> execute();
	$gigs = $statement -> fetchAll();
	$statement -> closeCursor();
	
	return $gigs;
	
}

function get_upcoming_events() {
	
	global $db;
	
	$sql = 'SELECT *
			FROM events e 
				JOIN gigs g
					ON e.gig_id = g.gig_id
				JOIN bands b
					ON e.band_id = b.band_id
				JOIN venues v
					ON g.gig_venue_id = v.venue_id
			WHERE gig_date > UNIX_TIMESTAMP(NOW())
			ORDER BY gig_date';
	
	$statement = $db -> prepare($sql);
	$statement -> execute();
	$events = $statement -> fetchAll();
	$statement -> closeCursor();
	
	return $events;
	
}

?>